import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { TradersComponent } from './traders/traders.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { Routes,RouterModule } from '@angular/router';

const routes: Routes = [
  {path :'',redirectTo:'home' , pathMatch:'full'},
  
  {path :'home',component:HomeComponent },
  {path :'traders',component:TradersComponent},
  {path :'about-us',component:AboutUsComponent },
  {path :'navbar',component:NavbarComponent },
  {path :'footer',component: FooterComponent },
  // {path :'**',component:PageNotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
